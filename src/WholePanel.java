import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

public class WholePanel extends JPanel {
	
	//initialize variables for GUI 
	private JScrollPane scrollPane;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JPanel textFileGroup;
	private JPanel dictionaryOptions;
	private JTextField textFileName;
	private JTextField dictionaryName;
	private JLabel errorMessage;
	private JList wordList;
	private DefaultListModel listModel;
	private JButton extractFilesButton;
	private JButton addToDictionary;
	private JButton ignoreWord;
	
	private Dictionary dictionary = new Dictionary(); //instantiate new Dictionary object 
	
	public WholePanel() {
		
		leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));
		
		rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
		
		JLabel textFile = new JLabel("Text File: ");
		JLabel dictionaryLabel = new JLabel("Dictionary: ");
		textFileName = new JTextField();
		dictionaryName = new JTextField();
		
		textFileGroup = new JPanel(new GridLayout(2, 2));
		textFileGroup.add(textFile);
		textFileGroup.add(textFileName);
		textFileGroup.add(dictionaryLabel);
		textFileGroup.add(dictionaryName);
		
		extractFilesButton = new JButton("Extract Files");
		errorMessage = new JLabel("");
		errorMessage.setForeground(Color.RED);
		
		//add 3 components to the left panel
		leftPanel.add(errorMessage);
		leftPanel.add(textFileGroup);
		leftPanel.add(extractFilesButton);
		
		listModel = new DefaultListModel();
		wordList = new JList(listModel);
		wordList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		wordList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		
		
		scrollPane = new JScrollPane(wordList);
		
		addToDictionary = new JButton("Add");
		ignoreWord = new JButton("Ignore");
		
		dictionaryOptions = new JPanel(new GridLayout(1, 1));
		dictionaryOptions.add(addToDictionary);
		dictionaryOptions.add(ignoreWord);
		
		//add 2 components to the right panel
		rightPanel.add(scrollPane);
		rightPanel.add(dictionaryOptions);
		
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);

		//add JSplitPane to the WholePanel
		add(sp);
		
		//instantiate ActionListener for the JButtons
		extractFilesButton.addActionListener(new ButtonListener());
		addToDictionary.addActionListener(new ButtonListener());
		ignoreWord.addActionListener(new ButtonListener());
		
	}
	
	
	private class ButtonListener implements ActionListener
	   {
	    public void actionPerformed(ActionEvent event)
	     {
	    	
	    	String buttonAction = event.getActionCommand();
	    	ArrayList<String> words = new ArrayList<String>();
	    	
	    	if(buttonAction.equals("Extract Files")) {
	    		
	    		String string1 = textFileName.getText();
	    		String string2 = dictionaryName.getText();

	    		System.out.println(string1);
				System.out.println(string2);

				if (string1.isEmpty() || string2.isEmpty()) {
	        		errorMessage.setText("Please fill all fields");
	    		}
	    		
	    		else {
	    			clearAllTextFields();
	    			errorMessage.setText("");
					try {
						BufferedReader in;
						File input = new File("input.txt");
						System.out.println(input.getAbsolutePath());
						in = new BufferedReader(new FileReader(input));
						String word;
	    				while((word = in.readLine()) != null){
	    					System.out.println("Added:"+word);
	    				    words.add(word);
	    				}
	    			in.close();
					} catch (FileNotFoundException e) {
						errorMessage.setText("Error: File not found");
					} catch (IOException e) {
						errorMessage.setText("Error: IO Exception error");
					}
					
					if (dictionary.isEmpty()) {
						
	    			try {
	    				BufferedReader in;
	    				File input = new File("input2.txt");
						in = new BufferedReader(new FileReader(input));
						String word;
					
	    			while((word = in.readLine()) != null){
	    			    dictionary.addToDictionary(word);
	    			}
	    			in.close();	
	    			} catch (FileNotFoundException e) {
	    				errorMessage.setText("Error: File not found");
	    			} catch (IOException e) {
						errorMessage.setText("Error: IO Exception error");
					}
	    			
					}
					//iterate through newly created array from input file and add words to JList that do not appear in dictionary
	    			for (int i = 0; i < words.size(); i++) {
	    				if (dictionary.isWordInDictionary(words.get(i)) == false) {
	    					listModel.addElement(words.get(i));	
	    				}
	    			}
	    		}
	        		
	    	}
	    	
	    	else if(buttonAction.equals("Add")) {
	    		
	    		//add selected word to the dictionary
	    		String selectedWord = (String)wordList.getSelectedValue();
	    		dictionary.addToDictionary(selectedWord);
	    		
	    		//remove selected word from the JList
	    		int index = wordList.getSelectedIndex();
	    		listModel.removeElementAt(index);	    		
	    		
	    	}
	    	
	    	else if(buttonAction.equals("Ignore")) {
	    		
	    		//remove selected word from the JList 
	    		int index = wordList.getSelectedIndex();
	    		listModel.removeElementAt(index);
	    	}
	     }
	   }
	
	public void clearAllTextFields() {
		
		//clear the text fields to prepare for next file extraction 
		textFileName.setText("");
		dictionaryName.setText("");
	}

}
