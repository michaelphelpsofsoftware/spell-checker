import java.util.HashMap;

public class Dictionary { //eventually switch dictionary ArrayList structure out with HashMap

    private HashMap<String, String> dictionary;

    public Dictionary() {

        dictionary = new HashMap<String, String>();

    }
    // Put new object to HashMap with empty string in key for now, perhaps in

    // future key will be definition

    public void addToDictionary(String word) {

        dictionary.put(word.toLowerCase(), "");

    }

    public boolean isWordInDictionary(String targetWord) {

        String word = dictionary.get(targetWord);

        if (word == "") {

            return true;

        }
        return false;

    }


    public boolean isEmpty()  {

        return dictionary.isEmpty();

    }

    public void removeAll() {

        dictionary.clear();

    }



}
