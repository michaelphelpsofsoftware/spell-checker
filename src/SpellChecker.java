import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;

public class SpellChecker
{
    //initialize variables for GUI

    private JScrollPane scrollPane;
    private JPanel leftPanel;
    private JPanel rightPanel;
    private JPanel textFileGroup;
    private JPanel dictionaryOptions;
    private JTextField textFileName;
    private JTextField dictionaryName;
    private JLabel errorMessage;
    private JList wordList;
    private DefaultListModel listModel;
    private JButton extractFilesButton;
    private JButton helpMenuButton;
    private JPopupMenu helpMenu;
    private JMenuItem menuItem;
    private JMenuItem menuItem1;
    private JMenuItem menuItem2;
    private JMenuItem menuItem3;
    private JMenuItem menuItem4;
    private String instructions;
    private String instructions1;
    private String instructions2;
    private String instructions3;
    private String instructions4;
    private JButton addToDictionary;
    private JButton ignoreWord;

    private Dictionary dictionary = new Dictionary(); //instantiate new Dictionary object



    // Program starts here. Create a new spellChecker object.

    public static void main(String[] args) {

        new SpellChecker();

    }



    // Load the frame and create the content.

    public SpellChecker() {

        JFrame frame = new JFrame("Spell Checker by Michael Phelps Software Inc.");

        JFrame.setDefaultLookAndFeelDecorated(true);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setComponents(frame.getContentPane());

        frame.pack();

        frame.setVisible(true);

        frame.setSize(450,500);



    }

    public void setComponents(Container pane) {

        leftPanel = new JPanel();

        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));



        rightPanel = new JPanel();

        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));



        JLabel textFile = new JLabel("Text file(s) (comma separated): ");

        JLabel dictionaryLabel = new JLabel("Dictionary: ");

        textFileName = new JTextField();

        dictionaryName = new JTextField();



        extractFilesButton = new JButton("Extract Files");



        JPanel placeholder = new JPanel();

        textFileGroup = new JPanel(new GridLayout(4, 2));

        textFileGroup.add(textFile);

        textFileGroup.add(textFileName);

        textFileGroup.add(dictionaryLabel);

        textFileGroup.add(dictionaryName);

        textFileGroup.add(placeholder);

        textFileGroup.add(extractFilesButton);



        errorMessage = new JLabel("");

        errorMessage.setForeground(Color.RED);



        //add 3 components to the left panel

        leftPanel.add(errorMessage);

        pane.add(textFileGroup, BorderLayout.NORTH);



        instructions =  "Type the name of the file you want to spell check.";

        instructions1 = "If theres more than one input file, separate them with a comma (NO SPACE!).";

        instructions2 = "Then, type in the name of the dictionary file you wish to write to.";

        instructions3 = "Last, click on each word and choose to add it or ignore it.";

        instructions4 = "You can select multiple words at a time by holding 'command'";



        helpMenuButton = new JButton("Help Menu");

        helpMenu = new JPopupMenu();

        menuItem = new JMenuItem(instructions);

        menuItem1 = new JMenuItem(instructions1);

        menuItem2 = new JMenuItem(instructions2);

        menuItem3 = new JMenuItem(instructions3);

        menuItem4 = new JMenuItem(instructions4);



        helpMenu.add(menuItem);

        helpMenu.add(menuItem1);

        helpMenu.add(menuItem2);

        helpMenu.add(menuItem3);

        helpMenu.add(menuItem4);

        ActionListener a = new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                showPopup(e);

            }

        };
        leftPanel.add(helpMenuButton);
        leftPanel.add(helpMenu);
        listModel = new DefaultListModel();
        wordList = new JList(listModel);
        wordList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        wordList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        scrollPane = new JScrollPane(wordList);
        addToDictionary = new JButton("Add");
        ignoreWord = new JButton("Ignore");

        dictionaryOptions = new JPanel(new GridLayout(1, 1));
        dictionaryOptions.add(addToDictionary);
        dictionaryOptions.add(ignoreWord);

        //add 2 components to the right panel
        rightPanel.add(scrollPane);
        JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);

        pane.add(sp, BorderLayout.CENTER);

        pane.add(dictionaryOptions, BorderLayout.SOUTH);

        //instantiate ActionListener for the JButtons

        extractFilesButton.addActionListener(new ButtonListener());
        addToDictionary.addActionListener(new ButtonListener());
        ignoreWord.addActionListener(new ButtonListener());
        helpMenuButton.addActionListener(new ButtonListener());
        helpMenuButton.addActionListener(a);

    }


    private class ButtonListener implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            String buttonAction = event.getActionCommand();
            ArrayList<String> words = new ArrayList<String>();
            if(buttonAction.equals("Extract Files")) {
            // Reset list model
            listModel.removeAllElements();
                //dictionary.removeAll();
                String string1 = textFileName.getText();
                String string2 = dictionaryName.getText();
                String[] string1arr = string1.split(",");
                System.out.println(string1);
                System.out.println(string2);

                if (string1.isEmpty() || string2.isEmpty()) {
                    errorMessage.setText("Please fill all fields");
                }

                else {
                    clearAllTextFields();
                    errorMessage.setText("");

                    try {
                        BufferedReader in;
                        for (int i = 0; i < string1arr.length; i++) {
                            File input = new File(string1arr[i]);
                            System.out.println(input.getAbsolutePath());
                            in = new BufferedReader(new FileReader(input));
                            String word;
                            while((word = in.readLine()) != null){

                                System.out.println("Added: " + word);
                                words.add(word.toLowerCase());

                            }
                            in.close();
                        }

                    } catch (FileNotFoundException e) {

                        errorMessage.setText("Error: File not found");

                    } catch (IOException e) {

                        errorMessage.setText("Error: IO Exception error");

                    }

                    if (dictionary.isEmpty()) {

                        try {

                            BufferedReader in;

                            File input = new File(string2);

                            in = new BufferedReader(new FileReader(input));

                            String word;



                            while((word = in.readLine()) != null){

                                dictionary.addToDictionary(word.toLowerCase());

                            }

                            in.close();

                        } catch (FileNotFoundException e) {

                            errorMessage.setText("Error: File not found");

                        } catch (IOException e) {

                            errorMessage.setText("Error: IO Exception error");

                        }



                    }

                    //iterate through newly created array from input file and add words to JList that do not appear in dictionary

                    for (int i = 0; i < words.size(); i++) {
                        if (!dictionary.isWordInDictionary(words.get(i)) && !listModel.contains(words.get(i))) {
                            System.out.println(dictionary.isWordInDictionary(words.get(i)));
                            listModel.addElement(words.get(i));
                        }

                    }

                }



            }

            else if(buttonAction.equals("Add")) {



                //add selected word to the dictionary

                String selectedWord = (String)wordList.getSelectedValue();

                int index = wordList.getSelectedIndex();



                if (selectedWord != null) {



                    if (!dictionary.isWordInDictionary(selectedWord)) {



                        dictionary.addToDictionary(selectedWord);

                        //remove selected word from the JList

                        listModel.removeElementAt(index);

                        errorMessage.setText("");

                    }

                    else {



                        errorMessage.setText("No duplicates!");

                        listModel.remove(index);



                    }

                }

            }



            else if(buttonAction.equals("Ignore")) {

                try {

                    //remove selected word from the JList

                    int index = wordList.getSelectedIndex();



                    listModel.removeElementAt(index);

                    errorMessage.setText("");



                } catch (ArrayIndexOutOfBoundsException e) {

                    errorMessage.setText("Please select a word from the list");

                }



            }



            else if(buttonAction.equals("Help Menu")) {



                showPopup(event);



            }

        }

    }



    public void clearAllTextFields() {

        //clear the text fields to prepare for next file extraction

        textFileName.setText("");

        dictionaryName.setText("");

    }



    private void showPopup(ActionEvent ae)

    {

        Component b=(Component)ae.getSource();



        Point p=b.getLocationOnScreen();



        helpMenu.show(leftPanel, 0, 0);

        helpMenu.setLocation(p.x,p.y+b.getHeight());

    }





}
